\documentclass[12pt,a4paper,oneside,hidelinks]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[catalan]{babel}
\usepackage{lmodern}
\usepackage{graphicx}
\usepackage[nottoc]{tocbibind}
\usepackage{url}
\usepackage{hyperref}
\usepackage{fullpage}
\usepackage{etoolbox}
\usepackage{caption}
\usepackage{minted}
\usepackage{verbatim}
\usepackage{color}
\usepackage{xcolor}
\usepackage{listings}
\usepackage{xparse}
\usepackage{pgfplots}
\apptocmd{\sloppy}{\hbadness 10000\relax}{}{}
\newcommand{\scare}[1]{`#1'}
\newcommand{\codeword}[1]{\texttt{\textcolor{blue}{\detokenize{#1}}}}
\pgfplotsset{compat=1.14}

\begin{document}

\pagenumbering{gobble}

\begin{titlepage}
	\centering
	\includegraphics[width=0.60\textwidth]{img/upc-positiu-p3005.png}
	\par
	\vspace{1cm}
	\large Universitat Politècnica de Catalunya - BarcelonaTech
	\par
	\large Facultat d'Informàtica de Barcelona
	\par
	\vspace{1cm}
	\large Grau en Enginyeria Informàtica
	\par
	\large Especialitat d'enginyeria de computadors
	\par
	\large Programació i Arquitectures Paral·leles
	\par
	\large 2016 Q2
	\par
	\vspace{1cm}
	\huge \textbf{Laboratori 2: Implementing a minimal OpenMP runtime}
	\par
	\vspace{0.5cm}
	\LARGE \textit{Itinerary 2: The tasking model}
	\par
	\vspace{2cm}
	\large \textit{Autor:}
	\par
	\large Sergi Granell Escalfet
	\par
	\vspace{1cm}
	\par
	\vfill
	\large \today \par
\end{titlepage}

\newpage
\tableofcontents
\newpage
\pagenumbering{arabic}

\section{Introducció}

L'objectiu d'aquest segon laboratori de l'assignatura de PAP és la implementació d'un \textit{runtime} reduït d'OpenMP. En el meu cas he triat realitzar el segon itinerari, és a dir, el del model de tasques.
\par
El sistema operatiu sobre el qual es realitzarà el desenvolupament i l'execució dels programes de prova/\textit{benchmarks} és Linux, i tot i que es podria utilitzar la crida al sistema \textit{futex()}, he decidit utilitzar el model de \textit{threading} estàndard \textit{pthreads} (POSIX Threads), ja que aquest és multiplataforma.

\section{Funcionalitats implementades}

S'ha de tenir en compte que tot i que l'enunciat limitava la implementació del \textit{runtime} a un sol nivell de parale\lgem{}isme i de tasques, la implementació realitzada suporta tan múltiples nivells de para\lgem{}elisme (\textit{nested parallelism}) i múltiples nivells de tasques niuades (\textit{nested tasks}). A més a més també s'ha implementat la construcció opcional \textit{taskloop}.

\subsection{Parallel regions}
En una regió parale\lgem{}a la funció \codeword{omp_get_thread_num()} ha de retornar l'identificador del \textit{thread} que la crida, i s'ha implementat mitjançant \codeword{pthread_key_t} (variables específiques per cada \textit{thread}).
\par
Per tal de crear una regió parale\lgem{}a, el compilador insereix una crida a la funció \codeword{GOMP_parallel()}. En aquesta funció es reserva un \textit{array} d'identificadors \codeword{pthread_t} i es creen els \textit{threads}, es crea una nova llista de tasques (\codeword{miniomp_tasklist_t}) i com que segons l'especificació d'OpenMP\footnote{\url{http://www.openmp.org/wp-content/uploads/openmp-4.5.pdf}} cada regió parale\lgem{}a consta d'una tasca implícita, també es crea aquesta i s'afegeix a la llista de tasques. Tot seguit es creen els \textit{threads} i s'envien a fer \textit{dispatching} de la llista de tasques. Llavors s'espera al fet que els \textit{threads} acabin mitjançant \codeword{pthread_join} i es destrueixen tots els recursos creats a la regió parale\lgem{}a.

\subsection{Task construct}

El compilador insereix crides a la funció \codeword{GOMP_task()} per tal de crear tasques.
\par
En aquesta funció es crea una nova instància del tipus \codeword{miniomp_task_t} i s'insereix a la llista de tasques actual.
El tipus \codeword{miniomp_task_t} consta dels següents camps:
\begin{minted}[frame=single, linenos, fontsize=\scriptsize]{c}
typedef struct miniomp_task_t {
	struct miniomp_task_t *parent;
	miniomp_tasklist_t *tasklist;
	void (*fn)(void *);
	void *data;
	uint32_t refs;
	uint32_t descendant_count;
	uint32_t children_count;
	uint32_t taskgroup_count;
	bool has_run;
	bool in_taskgroup;
	bool created_in_taskgroup;
	pthread_mutex_t mutex;
	pthread_cond_t cond;
	miniomp_list_t link;
} miniomp_task_t;
\end{minted}

Cal destacar el punter recursiu \codeword{parent} que apunta a la tasca pare o a \codeword{NULL} si és l'arrel (tasca implícita de la regió parale\lgem{}a), i per tant és amb el que es forma l'arbre de tasques. També hi trobem comptadors del nombre de descendents totals (útil per esperar al fet que totes les tasques descendents d'aquesta acabin), el nombre de fills directes, i nombre de fills creats en una directiva \codeword{taskloop}.
\par
Per tal de poder realitzar una espera passiva en \textit{dispatching} de la llista de tasques quan la llista està buida però encara hi ha tasques executant-se, s'utilitza la variable de condició \codeword{cond}.
\par
Quan es crea una nova tasca, es recorre l'arbre de tasques des de la tasca actual fins a l'arrel incrementant els comptadors corresponents: \codeword{descendant_count}, \codeword{children_count} només a la tasca pare de la qual s'acaba de crear, i \codeword{taskgroup_count} si aquesta s'ha creat a dins d'una directiva \textit{taskloop}, i a més a més es fa un \textit{broadcast} a la variable de condició \codeword{cond} per despertar tots els possibles \textit{threads} que estaven bloquejats fent \textit{dispatching}.
\par
Un cop un \textit{thread} ha executat la tasca obtinguda de la llista de tasques, es torna a recórrer l'arbre de tasques des de la tasca actual fins a l'arrel tot decrementant els comptadors corresponents: \codeword{descendant_count}, \codeword{children_count} només a la tasca pare de la qual s'acaba d'executar, i \codeword{taskgroup_count} si aquesta s'havia creat a dins d'una directiva \textit{taskloop}.

\subsection{Taskwait and taskgroup synchronizations}

La directiva \textit{taskwait} espera que totes les tasques filles directes de la tasca actual acabin. El compilador insereix una crida a \codeword{GOMP_taskwait()}. La implementació bloqueja afegint-se al \textit{dispatching} de la llista de tasques tot esperant que el comptador de nombre de tasques filles arribi a 0.
\par
El compilador insereix una parella de crides a les funcions \codeword{GOMP_taskgroup_start()} i \codeword{GOMP_taskgroup_end()} per tal de realitzar la directiva de tasques \textit{taskgroup}. A la funció \codeword{GOMP_taskgroup_start()} es posa el booleà \codeword{in_taskgroup} de la tasca a \textit{true}, així doncs, quan es crea una nova tasca filla se sap que aquesta pertany a un \textit{taskgroup}. Parale\lgem{}ament, a la funció \codeword{GOMP_taskgroup_end()} es posa el booleà a \textit{false} i es bloqueja fent \textit{dispatching} de la llista de tasques tot esperant que el comptador de nombre de tasques descendents creades al \textit{taskgroup} arribi a 0.

\subsection{Optional: taskloop construct}

Com ja s'ha mencionat anteriorment, en aquest \textit{runtime} també s'ha implementat la directiva opcional \textit{taskloop} amb les següents clàusules: \textit{grainsize} i \textit{num\_tasks}. El compilador insereix crides a la funció \codeword{GOMP_taskloop()}.
\par
Aquesta funció conté una sèrie de paràmetres, sent \codeword{flags}, \codeword{num_tasks}, \codeword{start}, \codeword{end}, i \codeword{step} els més rellevants. El paràmetre \codeword{flags} és una màscara de bits i indica quina si s'utilitza la clàusula \textit{grainsize}.
\par
En el cas que s'utilitzi \textit{grainsize}, el compilador passa aquest al paràmetre \codeword{num_tasks} i s'estableix el nombre de tasques a: $\lceil\frac{end - start}{step}\rceil$.
\par
D'altra banda, si no s'especifica \textit{grainsize}, si \codeword{num_tasks} és 0, aquest s'estableix a \codeword{omp_get_num_threads()}. El \textit{grainsize} es calcula com $\lceil\frac{end - start}{num\_tasks}\rceil$.
\par
Finalment es genera el nombre de tasques calculat prèviament, com si estiguessin en un \textit{taskloop} (segons l'especificació d'OpenMP) i s'insereixen a la cua, després s'afegeix el \textit{thread} actual al \textit{dispatching} de la cua de tasques esperant que les tasques generades acabin.

\section{Benchmarks}

\begin{figure}[H]
\centering
\begin{tikzpicture}
\begin{axis}[
	ylabel=Temps(s),
	xlabel=Nombre de \textit{threads},
	legend style={
		at={(1.3, 0.5)},
		anchor=north,legend columns=-1
	},
	ybar,
	xtick=data,
	symbolic x coords={1,2,4,8,16},
]
\addplot
	coordinates {(16, 0.8925204569473862)(8, 0.9019084278494119)(2, 1.697100531309843)(4, 0.9080966958776117)(1, 3.3763547938317062)};
\addplot
	coordinates {(16, 0.8958634415641427)(8, 0.8837107572704553)(2, 1.704512058570981)(4, 1.1084354108199477)(1, 3.391803921200335)};
\legend{miniomp,gomp}
\end{axis}
\end{tikzpicture}
\caption{tmandel2.c}
\label{fig_mandel}
\end{figure}

A la figura \ref{fig_mandel} podem observar com tant \textit{miniomp} com la implementació oficial del \textit{GCC} (\textit{gomp}) tenen un rendiment molt similar quan es creen poques tasques, i aquestes són molt grans (comportament de \textit{tmandel2}).


\begin{figure}[H]
\centering
\begin{tikzpicture}
\begin{axis}[
	ylabel=Temps(s),
	xlabel=Nombre de \textit{threads},
	legend style={
		at={(1.3, 0.5)},
		anchor=north,legend columns=-1
	},
	ybar,
	xtick=data,
	symbolic x coords={1,2,4,8,16},
]
\addplot
	coordinates {(16, 1.5549049241468311)(8, 1.0109606074169277)(2, 1.1783673947677016)(4, 1.0462222088128328)(1, 1.6628636192530393)};
\addplot
	coordinates {(16, 0.6964308826252819)(8, 0.7482060886919498)(2, 0.9692704750224947)(4, 0.7499584097415208)(1, 1.4009080378338694)};
\legend{miniomp,gomp}
\end{axis}
\end{tikzpicture}
\caption{sieve1\_taskloop.c, nombres enters fins a 100000000}
\label{fig_sieve1}
\end{figure}

En el cas de la figura \ref{fig_sieve1}, observem un comportament similar a les dues implementacions fins quan utilitzem uns 4 o 8 \textit{threads}: a mesura que augmentem el nombre de \textit{threads}, el temps d'execució disminueix, tot i que la nostra implementació té un rendiment inferior. Tot i això, la nostra implementació té una degradació de rendiment molt gran, fins al punt d'arribar quasi al temps d'execució amb un sol \textit{thread} quan n'utilitzem 16.

\begin{figure}[H]
\centering
\begin{tikzpicture}
\begin{axis}[
	ylabel=Temps(s),
	xlabel=Nombre de \textit{threads},
	legend style={
		at={(1.3, 0.5)},
		anchor=north,legend columns=-1
	},
	ybar,
	xtick=data,
	symbolic x coords={1,2,4,8,16},
]
\addplot
	coordinates {(16, 0.2827055500820279)(8, 0.240089113637805)(2, 0.36503613255918027)(4, 0.2378670297563076)(1, 0.39867597315460446)};
\addplot
	coordinates {(16, 0.23185801077634097)(8, 0.2178125711157918)(2, 0.20635986104607582)(4, 0.18214416243135928)(1, 0.3732653332874179)};
\legend{miniomp,gomp}
\end{axis}
\end{tikzpicture}
\caption{sieve2\_taskloop.c, nombres enters fins a 100000000, slice 5000000}
\label{fig_sieve2}
\end{figure}

A la figura \ref{fig_sieve2} hi observem de nou un comportament molt similar. Veiem com en les dues implementacions obtenim l'\textit{speed-up} màxim en utilitzar 4 \textit{threads}, i posteriorment aquest disminueix.

\section{Conclusions}

Jo crec que aquesta ha sigut una de les pràctiques més interessants que he fet fins ara a la FIB, no només pel repte que suposa programar utilitzant primitives de para\lgem{}elisme (\textit{threads}, \textit{mutex}, variables de condició, etc) sinó també pel fet d'haver implementat un \textit{runtime} d'OpenMP, que ja havíem començat a utilitzar a l'assignatura de PAR. Inicialment, al començar la implementació, em pensava que seria més senzill implementar el model de tasques, però ràpidament em vaig adonar que això no era així (en part per haver implementat tasques niuades), i que al darrere dels \textit{pragmes omp} hi ha algorismes i estructures de dades molt complexes.


\end{document}
