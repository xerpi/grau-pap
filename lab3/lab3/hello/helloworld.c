#include "mpi.h"
#include <stdio.h>
#ifdef _OPENMP
   #include <omp.h>
#endif

int main(int argc, char*argv[])
{
    int numtasks, rank, len, rc;
    char hostname[MPI_MAX_PROCESSOR_NAME];

    rc = MPI_Init(&argc,&argv);

    if (rc != MPI_SUCCESS) {
        printf("Error starting MPI program. Terminating.\n");
        MPI_Abort(MPI_COMM_WORLD,rc);
    }

    MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Get_processor_name(hostname,&len);
#ifdef _OPENMP
    #pragma omp parallel
    printf("Hello world from host %s, rank %d out of %d, thread %d out of %d\n",
            hostname,rank,numtasks,omp_get_thread_num(),omp_get_num_threads());
#else 
    printf("Hello world from host %s, rank %d out of %d\n",
            hostname,rank,numtasks);
#endif

    MPI_Finalize();
}
