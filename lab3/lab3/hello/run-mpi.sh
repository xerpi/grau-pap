#!/bin/bash

USAGE="\n USAGE: run-mpi program np\n
        program -> MPI program name \n
        np      -> number of MPI processes\n"

#Parameters
if (test $# -lt 2 || test $# -gt 2)
then
        echo -e $USAGE
        exit 0
fi

mpirun.mpich -np $2 -machinefile machines-mpi ./$1
