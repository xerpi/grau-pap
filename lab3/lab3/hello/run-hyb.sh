#!/bin/bash

USAGE="\n USAGE: run-mpi program np nt\n
        program -> MPI program name \n
        np      -> number of MPI processes\n
        nt      -> number of OpenMP threads\n"

#Parameters
if (test $# -lt 3 || test $# -gt 3)
then
        echo -e $USAGE
        exit 0
fi

mpirun.mpich -np $2 -machinefile machines-hyb -genv OMP_NUM_THREADS $3 ./$1
