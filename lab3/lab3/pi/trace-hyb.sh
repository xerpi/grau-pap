#!/bin/bash

USAGE="\n USAGE: run-mpi program size np nt\n
        program     -> MPI program name\n
        num_steps   -> number of steps\n
        np          -> number of MPI processes\n
        nt          -> number of OpenMP threads\n"


#Parameters
if (test $# -lt 4 || test $# -gt 4)
then
        echo -e $USAGE
        exit 0
fi

mpirun.mpich -np $3 -machinefile machines -genv OMP_NUM_THREADS $4 ./trace.sh ./$1 $2

# Copy intermediate trace files from other compute nodes
scp odroid@odroid-1:/home/odroid/paraver_trace/set-0/* /home/odroid/paraver_trace/set-0/.
scp odroid@odroid-2:/home/odroid/paraver_trace/set-0/* /home/odroid/paraver_trace/set-0/.
scp odroid@odroid-3:/home/odroid/paraver_trace/set-0/* /home/odroid/paraver_trace/set-0/.

# Create a directory to store the final Paraver trace
mkdir /sharedDir/pi/trace-$1-$2-$3-$4

# Generate the final Paraver trace
/home/odroid/extrae/bin/mpi2prv -f /home/odroid/paraver_trace/TRACE.mpits -o /sharedDir/pi/trace-$1-$2-$3-$4/trace.prv

# Clean up intermediate trace files and directories in all the compute nodes
rm -rf /home/odroid/paraver_trace /home/odroid/paraver_trace_tmp
ssh odroid@odroid-1 rm -rf /home/odroid/paraver_trace /home/odroid/paraver_trace_tmp
ssh odroid@odroid-2 rm -rf /home/odroid/paraver_trace /home/odroid/paraver_trace_tmp
ssh odroid@odroid-3 rm -rf /home/odroid/paraver_trace /home/odroid/paraver_trace_tmp

