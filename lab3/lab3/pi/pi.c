#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <unistd.h>
#include <omp.h>
#include "mpi.h"

double get_usecs()
{
    struct timeval time;
    gettimeofday(&time, NULL);
    return ((double)time.tv_sec * (double)1e6 + (double)time.tv_usec);
}

int main(int argc, char* argv[])
{
    int num_steps, chunk_size;
    int my_proc_id, num_procs;
    int my_start, my_end;
    double my_pi, pi, h, sum, x;
    double start_time = 0.0, end_time, exec_time;
    double PI25DT = 3.141592653589793238462643;
    int i;
    char hostname[MPI_MAX_PROCESSOR_NAME];
    int len;

    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&num_procs);
    MPI_Comm_rank(MPI_COMM_WORLD,&my_proc_id);
    MPI_Get_processor_name(hostname,&len);

    if (argc != 2) {
        printf("USAGE: pi num_steps\n");
        return 0;
    }

    num_steps = atoi(argv[1]);

    chunk_size = num_steps / num_procs;
    my_start = my_proc_id * chunk_size;
    my_end = (my_proc_id + 1) * chunk_size;

    //printf("Hello world from host %s, rank %d out of %d\n",hostname,my_proc_id,num_procs);

    MPI_Barrier(MPI_COMM_WORLD);

    if (my_proc_id == 0) {
    	start_time = get_usecs();
    }

    h   = 1.0 / (double) num_steps;
    sum = 0.0;

#pragma omp parallel for private(x) schedule(static) reduction(+:sum)
    for (i = my_start; i < my_end; ++i) {
        x = h * ((double)i - 0.5);
        sum += 4.0 / (1.0 + x*x);
    }

    my_pi = h * sum;

    MPI_Reduce(&my_pi, &pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    if (my_proc_id == 0) {

        end_time = get_usecs();
        exec_time = (end_time - start_time) / 1e6;

        printf("pi is approximately %.16f, Error is %.16f\n", pi, fabs(pi - PI25DT));
        printf("Execution time: %0.6f secs\n", exec_time);
    }

    MPI_Finalize();

    return 0;
}

