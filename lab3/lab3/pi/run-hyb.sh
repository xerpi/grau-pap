#!/bin/bash

USAGE="\n USAGE: run-mpi program size np nt\n
        program     -> MPI program name\n
        num_steps   -> number of steps\n
        np          -> number of MPI processes\n
        nt          -> number of OpenMP threads\n"

#Parameters
if (test $# -lt 4 || test $# -gt 4)
then
        echo -e $USAGE
        exit 0
fi

mpirun.mpich -np $3 -machinefile machines -genv OMP_NUM_THREADS $4 ./$1 $2
