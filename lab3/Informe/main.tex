\documentclass[12pt,a4paper,oneside,hidelinks]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[catalan]{babel}
\usepackage{lmodern}
\usepackage{graphicx}
\usepackage{tikz,pgfplots}
\usepackage[nottoc]{tocbibind}
\usepackage{url}
\usepackage{hyperref}
\usepackage{multirow}
\usepackage{fullpage}
\usepackage{etoolbox}
\usepackage{caption}
\usepackage{minted}
\apptocmd{\sloppy}{\hbadness 10000\relax}{}{}
\newcommand{\scare}[1]{`#1'}
\captionsetup[table]{position=bottom}

\begin{document}

\pagenumbering{gobble}

\begin{titlepage}
	\centering
	\includegraphics[width=0.60\textwidth]{img/upc-positiu-p3005.png}
	\par
	\vspace{1cm}
	\large Universitat Politècnica de Catalunya - BarcelonaTech
	\par
	\large Facultat d'Informàtica de Barcelona
	\par
	\vspace{1cm}
	\large Grau en Enginyeria Informàtica
	\par
	\large Especialitat d'enginyeria de computadors
	\par
	\large 2016 Q2
	\par
	\vspace{1cm}
	\LARGE Programació i Arquitectures Para"leles
	\par
	\vspace{1cm}
	\huge \textbf{Laboratori 3}
	\par
	\vspace{2cm}
	\large \textit{Autors:}
	\par
	\large Sergi Granell Escalfet
	\par
	\large Raúl Peñacoba Veigas
	\par
	\vspace{1cm}
	\par
	\vfill
	\large \today \par
\end{titlepage}

\newpage
\pagenumbering{arabic}

\section{Introducció}

L'objectiu d'aquest laboratori és utilitzar i realitzar proves de rendiment a un petit cluster HPC (High-Performance Computing) format per quatre plaques Odroid-XU4.\par
Cada una d'aquestes plaques disposa d'una arquitectura ARM big.LITTLE, que consisteix en 4 nuclis Cortex-A15 fora d'ordre i 4 nuclis ARM Cortex-A7 d'execució en ordre, a més a més de tenir 2GB de memòria LPDDR3 RAM.\par
Per tal de poder programar aquest clúster, s'utilitzarà el model de programació MPI (Message-Passing Interface) per a arquitectures NUMA (entre nodes) i OpenMP als nuclis de la CPU de cada node.

\section{Paral·lelització}
Un cop fet el \textit{set up} del cluster es compila un simple \textit{Hello World} que mostra l'identificador de cada procés MPI com els identificadors OpenMP dins de cada un d'aquests.\par
La repartició dels processos es realitza de forma cíclica i es configura dins un fitxer anomenat "machines-mpi". Per exemple, suposem que executarem 8 processos MPI. Un exemple de repartició pot ser el següent:

\begin{minted}
{bash}
odroid-0:1
odroid-1:1
odroid-2:1
odroid-3:1
\end{minted}

Això donarà com a resultat:

\begin{minted}
{bash}
Node -> Processos
odroid-0 -> 0, 4
odroid-1 -> 1, 5
odroid-2 -> 2, 6
odroid-3 -> 3, 7
\end{minted}

El programa sobre el qual realitzarem les proves calcula el nombre $\pi$ numèricament, realitzant tantes iteracions com l'argument passat a \verb|argv[1]| (\verb|num_steps|). Així doncs, com més iteracions es facin, més precisió tindrà el valor estimat de $\pi$.\par
Per tal de para"lelitzar el càlcul, en primer lloc es reparteixen \verb|num_steps| iteracions entre el nombre de nodes (\verb|chunk_size|) de forma estàtica, mitjançant MPI. Seguidament, cada node realitza les \verb|chunk_size| iteracions mitjançant un \verb|#pragma omp for| d'OpenMP, també de forma estàtica.\par
Amb l'objectiu de facilitar l'execució de l'aplicació, s'utilitza un \textit{script} que rep com a paràmetres el nombre de processos a crear (cada procés es mapeja a un node), i dins de cada procés, quants \textit{threads} utilitzar. La repartició dels processos a cada node es fa mitjançant el fitxer "machines-mpi" com s'ha explicat anteriorment.

\subsubsection{Apartat a)}
Com es pot veure a la taula \ref{table:1}, es produeix una millora de rendiment a causa del para"lelisme en un node. Cal destacar la poca millora que hi ha quan passem de 4 a 8 \textit{threads}, això és degut a l'arquitectura big.LITTLE, primer s'utilitzen els 4 cores més potents, i després es van afegint els cores menys potents.

\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|}
\hline
machines & \{process:thread\} & Temps (s) \\
\hline \hline
\multirow{4}{*}{1 1 1 1}
& \{1:1\} & 19.335273 \\ \cline{2-3}
& \{1:2\} & 9.670322 \\ \cline{2-3}
& \{1:4\} & 4.842248 \\ \cline{2-3}
& \{1:8\} & 4.052745 \\ \hline
\end{tabular}
\caption{}
\label{table:1}
\end{table}

\begin{center} \begin{tikzpicture} \begin{axis} [
    xlabel={Nombre de \textit{threads}},
    ylabel={Temps (s)},
    xtick={1, 2, 4, 8},
    legend pos=north east,
    ymajorgrids=true,
    grid style=dashed,
]
\addplot[
    color=blue,
    mark=square,
    ]
    coordinates {
      (1, 19.335273)(2, 9.670322)(4, 4.842248)(8, 4.052745)
    };
    \legend{machines: 1 1 1 1}
\end{axis} \end{tikzpicture} \end{center}


\subsubsection{Apartat b)}
Tot i la millora del temps d'execució que aconseguim l'apartat a), podem filar més prim aprofitant millor la potència dels nodes. Utilitzant el \verb|schedule static| la feina es repartirà equitativament entre els cores. Els més ràpids acabaran abans que els més lents, augmentant el temps d'execució.

Un \verb|schedule dynamic| sembla més adient, ja que la feina estarà disponible per qui estigui lliure. D'aquesta manera, els cores ràpids podran anar fent ``traient'' càrrega de treball als cores lents.


\subsubsection{Apartat c)}

Ara en comptes d'executar només un procés en un sol node, s'executen els processos repartint-los d'un a un a cada node de forma \textit{round-robin}, tot i que cada procés només consta d'un \textit{thread}. A la taula \ref{table:2} observem com en passar de 4 a 8 processos, el temps disminueix a la meitat (en contra del que passava abans). Això es deu al fet que ara quan es reparteixin els 8 processos, aniran 2 processos a cada node, i per tant aquests dos processos es podran executar als cores més potents.

\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|}
\hline
machines & \{process:thread\} & Temps (s) \\
\hline \hline
\multirow{4}{*}{1 1 1 1}
& \{1:1\} & 19.335492 \\ \cline{2-3}
& \{2:1\} & 9.701760 \\ \cline{2-3}
& \{4:1\} & 4.869576 \\ \cline{2-3}
& \{8:1\} & 2.448705 \\ \hline
\end{tabular}
\caption{}
\label{table:2}
\end{table}

\begin{center} \begin{tikzpicture} \begin{axis} [
    xlabel={Nombre de nodes},
    ylabel={Temps (s)},
    xtick={1, 2, 4, 8},
    legend pos=north east,
    ymajorgrids=true,
    grid style=dashed,
]
\addplot[
    color=blue,
    mark=square,
    ]
    coordinates {
      (1, 19.335492)(2, 9.701760)(4, 4.869576)(8, 2.448705)
    };
    \legend{machines: 1 1 1 1}
\end{axis} \end{tikzpicture} \end{center}


\subsubsection{Apartat d)}

Si posem el machines a \verb|8 8 8 8|, fins i tot quan reparteixin els 8 processos aniran a parar al mateix node, desaprofitant completament el para"lelisme entre nodes. A més a més, en el cas dels 8 processos, s'estaran utilitzant els cores menys potents. Això fa que hi hagi una degradació de rendiment en passar de 4 a 8 processos, tal com s'observa a la taula \ref{table:3}.

\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|}
\hline
machines & \{process:thread\} & Temps (s) \\
\hline \hline
\multirow{4}{*}{8 8 8 8}
& \{1:1\} & 19.338056 \\ \cline{2-3}
& \{2:1\} & 9.676243 \\ \cline{2-3}
& \{4:1\} & 4.841077 \\ \cline{2-3}
& \{8:1\} & 7.192779 \\ \hline
\end{tabular}
\caption{}
\label{table:3}
\end{table}

Per altra banda, si per exemple posem el machines a \verb|2 2 2 2|, (també serviria posar-lo a \verb|4 4 4 4|), evitem que s'arribin a utilitzar els cores menys potents, i d'aquesta forma no hi ha pèrdua de rendiment (taula \ref{table:4}).

\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|}
\hline
machines & \{process:thread\} & Temps (s) \\
\hline \hline
\multirow{4}{*}{2 2 2 2}
& \{1:1\} & 19.335612 \\ \cline{2-3}
& \{2:1\} & 9.673465 \\ \cline{2-3}
& \{4:1\} & 4.869709 \\ \cline{2-3}
& \{8:1\} & 2.450680 \\ \hline
\end{tabular}
\caption{}
\label{table:4}
\end{table}


\subsubsection{Apartat e)}

Tot i la variació de l'arxiu machines i la repartició \{process:thread\} que veiem a la taula \ref{table:5}, observem com els temps d'execució són pràcticament idèntics. Això es deu al fet que en tots els casos, tots els 8 \textit{threads} en total que es creen, siguin dins el mateix node o no, s'executen en els cores potents.

\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|}
\hline
machines & \{process:thread\} & Temps (s) \\
\hline \hline
4 1 1 1 & \{4:1\} & 4.842685 \\ \hline
1 1 1 1 & \{4:1\} & 4.866706 \\ \hline
1 1 1 1 & \{2:2\} & 4.866906 \\ \hline
2 1 1 1 & \{2:2\} & 4.843403 \\ \hline
1 1 1 1 & \{1:4\} & 4.841519 \\ \hline
\end{tabular}
\caption{}
\label{table:5}
\end{table}


\subsubsection{Apartat f)}

A les tres primeres files de la taula \ref{table:6} veiem un temps d'execució similar. El motiu d'això és que en aquests tres casos, en cap moment s'arriben a utilitzar els cores menys potents: en el primer cas van 2 processos d'1 \textit{thread} a cada node, en el segon cas van 2 processos de 2 \textit{threads} a cada node (per tant s'utilitzen tots els cores potents), i en el tercer cas van 2 processos de 4 \textit{threads} cada un a dos nodes diferents (també s'utilitzen tots els cores potents).\par
En el cas de l'última fila, va 1 procés amb 8 \textit{threads} a un node, i per tant s'utilitzen els cores menys potents, fet que comporta una pèrdua de rendiment.

\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|}
\hline
machines & \{process:thread\} & Temps (s) \\
\hline \hline
2 2 2 2 & \{8:1\} & 2.450779 \\ \hline
2 2 2 2 & \{4:2\} & 2.456078 \\ \hline
1 1 1 1 & \{2:4\} & 2.455966 \\ \hline
1 1 1 1 & \{1:8\} & 3.680326 \\ \hline
\end{tabular}
\caption{}
\label{table:6}
\end{table}

\subsubsection{Apartat g)}

La taula \ref{table:7} mostra els millors temps d'execució obtinguts. A les tres primeres files la distribució de la càrrega és si fa no fa la mateixa, ja que acaben utilitzant tots els cores potents de cada node, com es pot observar a la figura \ref{figure:16_1}. \par
Per altra banda, les dues últimes files no tenen gaire remei, ja que el nombre de threads per procés és més gran que el nombre de cores potents per node. Per tant, podem assegurar que els cores lents hauran de treballar empitjorant el rendiment.

\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|}
\hline
machines & \{process:thread\} & Temps (s) \\
\hline \hline
4 4 4 4 & \{16:1\} & 1.240218 \\ \hline
2 2 2 2 & \{8:2\} & 1.243200 \\ \hline
1 1 1 1 & \{4:4\} & 1.243071 \\ \hline
1 1 1 1 & \{2:8\} & 1.919660 \\ \hline
1 1 1 1 & \{1:16\} & 3.681580 \\ \hline
\end{tabular}
\caption{}
\label{table:7}
\end{table}

\subsubsection{Apartat h)}

En aquesta última taula \ref{table:8} s'utilitzen tots els cores dels nodes.\par
L'última fila obté el pitjor resultat, ja que no es poden executar tots els \textit{threads} a la vegada, i per tant hi ha serialització d'aquests.\par
La primera i penúltima fila obtenen resultats semblants, encara que els motius són diferents: el primer té una baixada de rendiment degut als cores lents que han d'acabar la feina, i el penúltim és que no es poden executar tots els \textit{threads} creats a la vegada, ja que només s'utilitzen dos dels nodes (figura \ref{figure:2_16}).\par
Els altres tres casos tenen bons resultats gràcies a l'\verb|schedule dynamic| d'OpenMP que permet als cores lents treure càrrega de treball als cores potents, mentre aquests últims treballen al màxim.\par
En el cas \{16:2\} observem una variació en el temps d'execució. Això es deu al fet que s'estan executant 4 processos de 2 \textit{threads} cada un, però el \verb|schedule dynamic| només afecta els \textit{threads} d'un mateix procés, i no als d'altres processos. Per tant a un procés li pot tocar executar tots els seus dos \textit{threads} en els cores més lents, fet que provoca que els processos en cores més ràpids acabin abans i MPI s'hagi d'esperar que acabin els processos en cores lents. Executant \textit{Paraver} es pot veure aquest fet a la figura \ref{figure:16_2}.

\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|}
\hline
machines & \{process:thread\} & Temps (s) \\
\hline \hline
8 8 8 8 & \{32:1\} & 1.877130 \\ \hline
4 4 4 4 & \{16:2\} & \textbf{1.048075} \\ \hline
4 4 4 4 & \{16:2\} & \textbf{1.280065} \\ \hline
2 2 2 2 & \{8:4\} & 1.062171 \\ \hline
1 1 1 1 & \{4:8\} & 1.042100 \\ \hline
1 1 1 1 & \{2:16\} & 1.850943 \\ \hline
1 1 1 1 & \{1:32\} & 3.688966 \\ \hline
\end{tabular}
\caption{}
\label{table:8}
\end{table}

\section{Conclusions}

En aquesta pràctica hem après com configurar un petit sistema HPC utilitzant nodes ARM big.LITTLE per tal de posar en marxa l'entorn d'execució MPI per arquitectures para"leles. No només això, sinó que també hem vist com para"lelitzar un programa que computa dígits de $\pi$ barrejant OpenMP i MPI, i com en funció de la repartició de processos i \textit{threads} entre els diferents nodes pot fer variar molt notablement el rendiment d'aquest.

\section{Apèndix: Traces \textit{Paraver}}

\begin{center}
\includegraphics[width=1.0\textwidth]{img/16-1.jpg}
\captionof{figure}{\{16:1\}}
\label{figure:16_1}
\end{center}

\begin{center}
\includegraphics[width=1.0\textwidth]{img/16-2.jpg}
\captionof{figure}{\{16:2\}}
\label{figure:16_2}
\end{center}

\begin{center}
\includegraphics[width=1.0\textwidth]{img/2-16.jpg}
\captionof{figure}{\{2:16\}}
\label{figure:2_16}
\end{center}

\end{document}
