#!/bin/bash
export SOFT_ROOT=/Soft/PAR

# Extrae
export EXTRAE_HOME=$SOFT_ROOT/extrae/3.4.1
export PATH=$EXTRAE_HOME/bin/:$PATH
export LD_LIBRARY_PATH=/Soft/libunwind/current/lib/:$EXTRAE_HOME/lib:$LD_LIBRARY_PATH
export EXTRAE_LABELS=./user-events.dat
export EXTRAE_ON=TRUE
export EXTRAE_CONFIG_FILE=./extrae.xml

# Paraver
export PARAVER_HOME=$SOFT_ROOT/paraver/4.5.1
export PATH=$PARAVER_HOME/bin:$PATH

# New version of gcc supporting OpenMP 4.5
export PATH=/Soft/gcc/6.2.0/bin:$PATH
export LD_LIBRARY_PATH=/Soft/gcc/6.2.0/lib64:$LD_LIBRARY_PATH

# Miniomp library
export MINIOMP=$HOME/lab2/miniomp
export LD_LIBRARY_PATH=$MINIOMP/lib:$LD_LIBRARY_PATH

export LC_ALL="en_US.utf-8"

