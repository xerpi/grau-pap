USAGE="\n USAGE: ./trace-hybrid.sh executable nprocs threadsxproc\n
        executable    -> MPI executable file\n
        nprocs        -> Total number MPI of processes\n
        threadsxproc  -> Number of OpenMP threads per MPI process\n"


if (test $# -lt 3 || test $# -gt 3)
then
        echo -e $USAGE
        exit 0
fi

make $1

export EXTRAE_HOME=/home/odroid/extrae
export EXTRAE_CONFIG_FILE=extrae.xml
export LD_PRELOAD=$EXTRAE_HOME/lib/libompitrace.so

mpirun.mpich -np $2 -ppn 1 -machinefile machines -genv OMP_NUM_THREADS $3 ./$1 test.dat

# Copy intermediate trace files from other compute nodes
scp odroid@odroid-1:/home/odroid/paraver_trace/set-0/* /home/odroid/paraver_trace/set-0/.
scp odroid@odroid-2:/home/odroid/paraver_trace/set-0/* /home/odroid/paraver_trace/set-0/.
scp odroid@odroid-3:/home/odroid/paraver_trace/set-0/* /home/odroid/paraver_trace/set-0/.

# Create a directory to store the final Paraver trace
mkdir /sharedDir/heat/trace-$1-$2-$3

# Generate the final Paraver trace
/home/odroid/extrae/bin/mpi2prv -f /home/odroid/paraver_trace/TRACE.mpits -o /sharedDir/heat/trace-$1-$2-$3/trace.prv

# Clean up intermediate trace files and directories in all the compute nodes
rm -rf /home/odroid/paraver_trace /home/odroid/paraver_trace_tmp
ssh odroid@odroid-1 rm -rf /home/odroid/paraver_trace /home/odroid/paraver_trace_tmp
ssh odroid@odroid-2 rm -rf /home/odroid/paraver_trace /home/odroid/paraver_trace_tmp
ssh odroid@odroid-3 rm -rf /home/odroid/paraver_trace /home/odroid/paraver_trace_tmp
