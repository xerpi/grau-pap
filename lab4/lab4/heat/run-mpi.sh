USAGE="\n USAGE: ./run-mpi.sh executable nodes procsxnode\n
        executable    -> MPI executable file\n
        nodes         -> Number of nodes\n
        procsxnode    -> Number of processes per node\n"

if (test $# -lt 3 || test $# -gt 3)
then
        echo -e $USAGE
        exit 0
fi

make $1

export nprocs=`echo "$2 * $3" | bc -l`

mpirun.mpich -np $nprocs -ppn $3 -machinefile machines ./$1 test.dat
