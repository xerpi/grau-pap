USAGE="\n USAGE: ./run-hybrid.sh executable nprocs threadsxproc\n
        executable    -> MPI executable file\n
        nprocs        -> Total number MPI of processes\n
        threadsxproc  -> Number of OpenMP threads per MPI process\n"


if (test $# -lt 3 || test $# -gt 3)
then
        echo -e $USAGE
        exit 0
fi

make $1

mpirun.mpich -np $2 -ppn 1 -machinefile machines -genv OMP_NUM_THREADS $3 ./$1 test.dat
