#include "heat.h"

/*
 * Blocked Jacobi solver: one iteration step
 */
double relax_jacobi(double *u, double *utmp, unsigned sizex, unsigned sizey)
{
	int i, j;
	double diff, sum = 0.0;

	#pragma omp parallel for schedule(dynamic) private(i, j, diff) reduction(+:sum)
	for (i = 1; i < sizey - 1; i++) {
		for (j = 1; j < sizex - 1; j++) {
			utmp[i * sizey + j] = 0.20 * (u[i       * sizey + j] +        // center
						      u[i       * sizey + (j - 1)] +  // left
						      u[i       * sizey + (j + 1)] +  // right
						      u[(i - 1) * sizey + j] +        // top
						      u[(i + 1) * sizey + j]);        // bottom

			diff = utmp[i * sizey + j] - u[i * sizey + j];
			sum += diff * diff;
		}
	}

	return sum;
}
