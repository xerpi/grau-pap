\documentclass[12pt,a4paper,oneside,hidelinks]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[catalan]{babel}
\usepackage{lmodern}
\usepackage{graphicx}
\usepackage{tikz,pgfplots}
\usepackage[nottoc]{tocbibind}
\usepackage{url}
\usepackage{hyperref}
\usepackage{multirow}
\usepackage{fullpage}
\usepackage{etoolbox}
\usepackage{caption}
\usepackage{minted}
\usepackage{blindtext}
\usepackage{scrextend}
\usepackage{listings}
\addtokomafont{labelinglabel}{\sffamily}
\apptocmd{\sloppy}{\hbadness 10000\relax}{}{}
\newcommand{\scare}[1]{`#1'}
\captionsetup[table]{position=bottom}
\lstset{basicstyle=\ttfamily}

\begin{document}

\pagenumbering{gobble}

\begin{titlepage}
	\centering
	\includegraphics[width=0.60\textwidth]{img/upc-positiu-p3005.png}
	\par
	\vspace{1cm}
	\large Universitat Politècnica de Catalunya - BarcelonaTech
	\par
	\large Facultat d'Informàtica de Barcelona
	\par
	\vspace{1cm}
	\large Grau en Enginyeria Informàtica
	\par
	\large Especialitat d'enginyeria de computadors
	\par
	\large 2016 Q2
	\par
	\vspace{1cm}
	\LARGE Programació i Arquitectures Para"leles
	\par
	\vspace{1cm}
	\huge \textbf{Laboratori 4}
	\par
	\vspace{2cm}
	\large \textit{Autors:}
	\par
	\large Sergi Granell Escalfet
	\par
	\large Raúl Peñacoba Veigas
	\par
	\vspace{1cm}
	\par
	\vfill
	\large \today \par
\end{titlepage}

\newpage
\pagenumbering{arabic}

\section{Introducció}


En aquesta sessió es paral·lelitzarà un codi seqüencial que simula la difusió de la calor a un cos sòlid utilitzant el mètode Jacobi.

Aquest programa utilitza un fitxer de configuració on és es defineix el màxim nombre d'iteracions de l'algorisme, la mida del cos, i les fonts de calor amb posició, mida i temperatura.

El resultat és una imatge en \textit{portable pixmap file format} indicant la temperatura amb un gradient vermell - blau.


\section{Repartint les dades}

Per tal d'aconseguir una versió paral·lelitzada funcional primer hem de dividir les dades, en aquest cas, la matriu. El procés zero (\textit{master}) és l'encarregat de dur a terme aquesta tasca. El codi associat és el següent:

\begin{minted}[frame=single, fontsize=\scriptsize]{c}
// Enviar les dades
for (int i = 1; i < numprocs; i++) {
	MPI_Send(&maxiter, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
	MPI_Send(&columns, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
	MPI_Send(&rows, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
	MPI_Send(&u[columns_2 * rows * i], rows_2 * columns_2, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
	MPI_Send(&uhelp[columns_2 * rows * i], rows_2 * columns_2, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
}

// Càlcul de relax jacobi
...

// Rebre els resultats
for (int i = 1; i < numprocs; i++) {
	MPI_Recv(&u[columns_2 * rows * i], rows * columns_2, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, &status);
}
\end{minted}

Les variables més importants d'aquest codi són:
\begin{labeling}{labeling1}
\item [columns] la resolució, indicada al fitxer de configuració.
\item [rows] nombre de files per processador. Es calcula com resolució/numprocs
\item [columns\_2] igual que columns però tenint en compte els boundaries. Es calcula com columns + 2
\item [rows\_2] igual que rows però tenint en compte els boundaries. Es calcula com rows + 2
\end{labeling}

Per altra banda, el codi que executen els altres nodes (\textit{workers}) és el següent:

\begin{minted}[frame=single, fontsize=\scriptsize]{c}
MPI_Recv(&maxiter, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
MPI_Recv(&columns, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
MPI_Recv(&rows, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);

// Fill initial values for matrix with values received from master
MPI_Recv(&u[0], rows_2 * columns_2, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);
MPI_Recv(&uhelp[0], rows_2 * columns_2, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &status);

// Càlcul de relax jacobi
...

// Send the result to proc. zero
MPI_Send(&u[columns_2], rows * columns_2, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
\end{minted}

\section{Comunicació entre processos}

La versió anterior no és correcta, ja que a cada iteració no es propaguen els \textit{boundaries}, que són dependències, entre processos consecutius.

En aquest apartat incloem el pas d'aquesta informació. Tots els processadors executen el mateix a cada iteració de càlcul de la matriu:

\begin{minted}[frame=single, fontsize=\scriptsize]{c}
// Enviar l'upper boundary al lower boundary del processador de dalt
if (myid > 0) {
	MPI_Sendrecv(&u[columns_2], columns_2, MPI_DOUBLE, myid - 1, 0,
                     &u[0], columns_2, MPI_DOUBLE, myid - 1, 0,
                     MPI_COMM_WORLD, &status);
}

// Enviar el lower boundary al upper boundary del processador de baix
if (myid < numprocs - 1) {
	MPI_Sendrecv(&u[rows * columns_2], columns_2, MPI_DOUBLE, myid + 1, 0,
                     &u[(rows + 1) * columns_2], columns_2, MPI_DOUBLE, myid + 1, 0,
                     MPI_COMM_WORLD, &status);
}
\end{minted}

\section{Decidir quan acabar}

La versió actual sembla que funciona correctament, però falta un detall important. Ara l'algorisme finalitza quan un nombre d'iteracions fix s'assoleix. Per tal d'aconseguir un resultat equiparable a la versió seqüencial hem d'utilitzar la variable \lstinline|residual|. En el cas de la versió MPI hem d'afegir la comunicació necessària per agrupar mitjançant una reducció (\lstinline|MPI_Reduce|) i distribuir (\lstinline|MPI_Bcast|) la suma de cada \lstinline|residual| calculat per cada processador.

El codi encarregat de dur aquesta tasca, també dins el \textit{loop}, és el següent:
\begin{minted}[frame=single, fontsize=\scriptsize]{c}
// Fem una reducció de tipus suma al procés zero i la distribuïm a la resta
MPI_Reduce(&residual, &global_residual, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
MPI_Bcast(&global_residual, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
\end{minted}

\section{Resultats}

Utilitzant l'última versió del codi MPI i el fitxer d'entrada \lstinline|eval.dat|, obtenim els següents resultats d'execució:

% ex1 pagina iv)
\begin{table}[H]
\centering
\begin{tabular}{|c|c|}
\hline
\{nodes:processos per node\} & Temps (s) \\
\hline \hline
\{1:4\} & 14.116 \\ \hline
\{4:1\} & 13.876 \\ \hline
\end{tabular}
\caption{}
\label{table:1}
\end{table}

Com podem observar a la taula \ref{table:1} no hi ha diferències significatives entre les dues versions, ja que en essència és el mateix: en total s'utilitzen 4 \textit{cores} ràpids.

Traient traces \textit{Paraver} d'algunes versions per veure què succeeix internament obtenim el següent:

% ex2 pagina iv)
\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{img/trace-heat-ex5-mpi-4-1.png}
\caption{4 nodes, 1 procés a cada node}
\label{figure:4_1}
\end{figure}

% ex3 pagina iv)
\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{img/trace-heat-ex5-mpi-1-8.png}
\caption{1 node, 8 processos a cada node}
\label{figure:1_8}
\end{figure}

Com s'intuïa, no s'aprecia cap diferència entre fer l'execució dels processos dins només un node o en diferents, és a dir, els dos segueixen els patrons de comunicació entre processos esperats.\par

Els temps d'execució que han trigat diverses combinacions nodes - processos per node han estat els següents:

\begin{table}[H]
\centering
\begin{tabular}{|c|c|}
\hline
\{nodes:processos per node\} & Temps (s) \\
\hline \hline
\{1:1\} & 35.071 \\ \hline
\{1:2\} & 21.812 \\ \hline
\{2:1\} & 23.585 \\ \hline
\{1:4\} & 14.578 \\ \hline
\{2:2\} & 14.455 \\ \hline
\{4:1\} & 13.863  \\ \hline
\{1:8\} & 15.157 \\ \hline
\{2:4\} & 9.466 \\ \hline
\{4:2\} & 9.369 \\ \hline
\{2:8\} & 11.152 \\ \hline
\{4:4\} & 7.798 \\ \hline
\{4:8\} & 10.018 \\ \hline
\end{tabular}
\caption{}
\label{table:2}
\end{table}

Afegim un gràfic dels \textit{speed-ups} relatius a l'execució seqüencial (\{1:1\}) pels diferents parells mostrats a la taula \ref{table:2}:

%ex4 pagina iv)
\begin{figure}[H]
\centering
\begin{tikzpicture}
\begin{axis}[
	ylabel=Speed-up,
	xlabel=\{nodes:procesos per node\},
	xtick=data,
	xticklabels={{1:1}, {1:2}, {2:1}, {1:4}, {2:2}, {4:1}, {1:8}, {2:4}, {4:2}, {2:8}, {4:4}, {4:8}},
	ybar,
	width=12cm,
]
\addplot
	coordinates {
	    (0,1)
	    (1,1.60787639831286)
	    (2,1.48700445198219)
	    (3,2.40574838798189)
	    (4,2.42621930127983)
	    (5,2.52982759864387)
	    (6,2.31384838688395)
	    (7,3.70494401014156)
	    (8,3.74330238018999)
	    (9,3.14481707317073)
	    (10,4.49743523980508)
	    (11,3.50079856258734)};
\end{axis}
\end{tikzpicture}
\caption{\textit{Speed-ups} utilitzant MPI}
\label{figure:scalability_plot}
\end{figure}

\section{Let's go hybrid}

Per millorar encara més el rendiment es pot utilitzar OpenMP dins cada procés MPI. Les modificacions per afegir OpenMP les hem fet a la funció \lstinline|relax\_jacobi| de la següent forma:
\begin{minted}[frame=single, fontsize=\scriptsize]{c}
double relax_jacobi(double *u, double *utmp, unsigned sizex, unsigned sizey)
{
	int i, j;
	double diff, sum = 0.0;

	#pragma omp parallel for schedule(dynamic) private(i, j, diff) reduction(+:sum)
	for (i = 1; i < sizey - 1; i++) {
		for (j = 1; j < sizex - 1; j++) {
		    /* Codi que realitza el mètode de Jacobi */
		}
	}

	return sum;
}
\end{minted}

En primer lloc utilitzem l'\textit{scheduling} dinàmic per tal d'aprofitar el fet que cada node té 4 \textit{cores} ràpids i 4 de més lents, i per tant si utilitzéssim un \textit{scheduling} estàtic, els 4 \textit{cores} ràpids acabarien abans i s'estarien en \textit{idle} esperant a que la resta acabi. Les variables \lstinline|i|, \lstinline|j| i \lstinline|diff| són privades per cada \textit{thread} d'OpenMP. La variable \lstinline|sum| és compartida, i per tal d'evitar condicions de carrera, aprofitant que s'hi acumulen sumes, utilitzem la clàusula \lstinline|reduction| d'OpenMP.\par

La següent gràfica mostra l'\textit{speedup} assolit comparant amb la versió seqüencial. Aclarim que la versió híbrida només té un procés per node, i aquest procés és el que crea els \textit{threads}.

\begin{figure}[H]
\centering
\begin{tikzpicture}
\begin{axis}[
	ylabel=Speed-up,
	xlabel=\{nodes:threads omp per node\},
	xtick=data,
	xticklabels={{1:1}, {1:2}, {1:4}, {1:8}, {2:1}, {2:2}, {2:4}, {2:8}, {4:1}, {4:2}, {4:4}, {4:8}},
	ybar,
	width=12cm,
]
\addplot
	coordinates {
	    (0,1)
	    (1,1.9216986301369863)
	    (2,3.7800172450959253)
	    (3,4.352860866327417)
	    (4,1.3801503285978511)
	    (5,2.5136897935779814)
	    (6,4.342620108964834)
	    (7,4.4416160081053695)
	    (8,2.349973197534173)
	    (9,3.5590623097219405)
	    (10,4.858825159323912)
	    (11,4.984508243320068)};
\end{axis}
\end{tikzpicture}
\caption{\textit{Speed-ups} utilitzant MPI i OpenMP}
\label{figure:scalability_plot_omp}
\end{figure}

Es pot apreciar com les versions que utilitzen el màxim de \textit{threads} en paral·lel possibles tenen més bons resultats. A més, sembla que dins d'aquesta distinció es millora el temps d'execució a l'augmentar el nombre de nodes, encara que és menys notòria.

\iffalse
SEQ -> 35.071
-------- Hybrid -----------
1 2 -> 18.250
1 4 ->  9.278
1 8 ->  8.057

2 1 -> 25.411
2 2 -> 13.952
2 4 ->  8.076
2 8 ->  7.896

4 1 -> 14.924
4 2 ->  9.854
4 4 ->  7.218
4 8 ->  7.036
--- MPI only ----
1 2: 21.812
1 4: 14.578
1 8: 15.157

2 1: 23.585
2 2: 14.455
2 4: 9.466
2 8: 11.152

4 1: 13.863 
4 2: 9.369
4 4: 7.798
4 8: 10.018
\fi

A la següent gràfica hi podem observar l'\textit{overhead} que causa utilitzar processos en comptes de \textit{threads}, ja que li és més costós al sistema operatiu realitzar el canvi de context. Cal tenir en compte que quan s'utilitzen 8 processos MPI es pot considerar que l'\textit{scheduling} realitzat en la repartició de la feina és estàtic, en contra de quan s'utilitzen 8 \textit{threads}, que els hem programat amb \textit{scheduling} dinàmic.

\begin{figure}[H]
\centering
\begin{tikzpicture}
\begin{axis}[
	ylabel=Temps(s),
	xlabel=\{nodes:processos/threads per node\},
	legend style={
		at={(0.9, 1.0)},
		anchor=north
	},
	xtick=data,
	xticklabels={{1:2}, {1:4}, {1:8}, {2:1}, {2:2}, {2:4}, {2:8}, {4:1}, {4:2}, {4:4}, {4:8}},
	ybar,
	width=15cm,
	ymin=0,
]
\addplot
	coordinates { % MPI only
	    (0,21.812)
	    (1,14.578)
	    (2,15.157)
	    (3,23.585)
	    (4,14.455)
	    (5,9.466)
	    (6,11.152)
	    (7,13.863)
	    (8,9.369)
	    (9,7.798)
	    (10,10.018)	    
	    };
\addplot
	coordinates { % Hybrid
	    (0,18.250)
	    (1,9.278)
	    (2,8.057)
	    (3,25.411)
	    (4,13.952)
	    (5,8.076)
	    (6,7.896)
	    (7,14.924)
	    (8,9.854)
	    (9,7.218)
	    (10,7.036)
	    };
\legend{MPI sol,híbrid}
\end{axis}
\end{tikzpicture}
\caption{MPI sol vs híbrid}
\label{figure:mpi_vs_hybrid}
\end{figure}

\section{Conclusions}

Tant MPI com OpenMP són dos APIs que ens permeten paral·lelitzar codi per tal d'aprofitar al màxim els recursos de la màquina, principalment els \textit{cores} de les CPUs. Creiem que la forma d'obtenir el màxim rendiment és combinar aquestes dues APIs utilitzant MPI per repartir feina entre nodes, i OpenMP per repartir la feina de cada node entre els \textit{threads} d'aquest.


\end{document}