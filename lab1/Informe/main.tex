\documentclass[12pt,a4paper,oneside,hidelinks]{article}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[catalan]{babel}
\usepackage{lmodern}
\usepackage{graphicx}
\usepackage[nottoc]{tocbibind}
\usepackage{url}
\usepackage{hyperref}
\usepackage{fullpage}
\usepackage{etoolbox}
\usepackage[font={footnotesize,it}]{caption}
\usepackage{minted}
\apptocmd{\sloppy}{\hbadness 10000\relax}{}{}
\newcommand{\scare}[1]{`#1'}

\begin{document}

\pagenumbering{gobble}

\begin{titlepage}
	\centering
	\includegraphics[width=0.60\textwidth]{img/upc-positiu-p3005.png}
	\par
	\vspace{1cm}
	\large Universitat Politècnica de Catalunya - BarcelonaTech
	\par
	\large Facultat d'Informàtica de Barcelona
	\par
	\vspace{1cm}
	\large Grau en Enginyeria Informàtica
	\par
	\large Especialitat d'enginyeria de computadors
	\par
	\large Programació i Arquitectures Paral·leles
	\par
	\large 2016 Q2
	\par
	\vspace{1cm}
	\huge \textbf{Laboratori 1}
	\par
	\vspace{2cm}
	\large \textit{Autor:}
	\par
	\large Sergi Granell Escalfet
	\par
	\vspace{1cm}
	\par
	\vfill
	\large \today \par
\end{titlepage}

\newpage
\tableofcontents
\newpage
\pagenumbering{arabic}

\section{Introducció}

Aquest és el primer laboratori de l'assignatura PAP de l'especialitat d'Enginyeria de Computadors del Grau en Enginyeria Informàtica de la Facultat d'Informàtica de Barcelona.\par
L'objectiu d'aquest laboratori és recordar alguns dels pragmas i construccions bàsiques d'OpenMP, a més a més de la utilització de Paraver per tal de poder veure i analitzar traces d'execució dels programes paral·lels.\par
El programa que utilitzarem com a base per les optimitzacions calcula el número de nombres primers que hi han fins a un cert número (passat com a paràmetre) mitjançant l'algorisme de la Criba de Eratóstenes.\par
En primer lloc hem de paral·lelitzar el programa sieve1.c mitjançant construccions \textit{for} i \textit{taskloop}.\par

\section{Paral·lelitzant \textit{sieve1.c}}

\subsection{Estratègies de paral·lelització}

\subsubsection{Utilitzant \textit{for}}

En primer lloc optimitzarem el programa \textit{sieve1.c} mitjançant les directives \textit{for}.\par
El primer \textit{for} de la línia 10 és trivial, ja que no hi han dependències, per tant amb l'\textit{schedule static} per defecte n'hi ha més que suficient.\par
El for de la línia 14 té dependències entre les iteracions (array \textit{isPrime}), i si es paral·lelitzés un thread accediria a un \textit{isPrime[i]} que en el moment de l'accés seria 1, però un altre thread el posaria 0 més endavant, i per tant s'estaria fent feina inútil i és millor no paral·lelitzar aquest for. Per altra banda, el for de la línia 19 si que es pot paral·lelitzar fàcilment, ja que un cop trobat un \textit{isPrime[i] == 1}, hem de descartar tots els múltiples d'aquest.\par
Finalment apliquem una reducció del tipus suma al for de la línia 28 per tal de contar el nombre de números primer trobats (accedint al array \textit{isPrime}).

\begin{minted}[frame=single, linenos, fontsize=\scriptsize]{c}
int eratosthenes(int lastNumber)
{
        int found = 0;
        int sqrt_lN = sqrt(lastNumber);

        // 1. create a list of natural numbers 2, 3, 4, ... all of them initially marked as potential primes
        char *isPrime = (char *)malloc((lastNumber + 1) * sizeof(char));

        #pragma omp parallel for
        for (int i = 0; i <= lastNumber; i++)
                isPrime[i] = 1;

        // 2. Starting from i=2, the first unmarked number on the list ...
        for (int i = 2; i <= sqrt_lN; i++) {
                //for (int i = 2; i*i <= lastNumber; i++)
                // ... find the smallest number greater or equal than i that is unmarked
                if (isPrime[i]) {
                        // 3. Mark all multiples of i between i^2 and lastNumber
                        #pragma omp parallel for
                        for (int j = i * i; j <= lastNumber; j += i) {
                                isPrime[j] = 0;
                        }
                }
        }
        
        // 4. The unmarked numbers are primes, count primes
        #pragma omp parallel for reduction(+:found)
        for (int i = 2; i <= lastNumber; i++)
                found += isPrime[i];

        // 5. We are done with the isPrime array, free it
        free(isPrime);
        return found;
}
\end{minted}

\subsubsection{Utilitzant \textit{taskloop}} \label{sieve1:taskloop}

La directiva \textit{taskloop} és nova a OpenMP 4.5, i permet crear tasques a partir de les iteracions d'un bucle. A més a més té una barrera de \textit{tasks} implícita al final (equivalent a \textit{taskgroup}).\par
Paral·lelitzar el primer i segon bucle \textit{for} mitjançant \textit{taskloop} és trivial, ja que no hi ha dependències entre les iteracions, i per tant es poden fer completament en paral·lel.\par
Cal destacar l'ús del \textit{taskloop}:
\begin{enumerate}
	\item Es crea una regió paral·lela amb \textit{\#pragma omp parallel}.
	\item Només es permet que un \textit{thread} executi la regió paral·lela per tal de crear les tasques només un cop (\textit{\#pragma omp single}).
	\item Finalment s'utilitza \textit{\#pragma omp taskloop} per crear les tasques a partir de les iteracions del bucle. Com que està en una regió paral·lela, els \textit{threads} comencen a processar les tasques, i s'esperen al \textit{barrier} de tasques implícit al final del \textit{taskloop}.
\end{enumerate}

Pel que fa al tercer bucle (el que compta el número de nombres primers), creem una variable local per cada \textit{thread} utilitzant un \textit{array}. Per tal d'evitar \textit{false sharing}, hem de deixar \textit{padding} per tal que cada variable local vagi a una linia de \textit{cache} diferent. Podem saber el tamany de linia de \textit{cache} L1 de dades amb la següent comanda: \begin{verbatim}cat /sys/devices/system/cpu/cpu0/cache/index0/coherency_line_size\end{verbatim}
En el cas del boada, la línia és de 64 bytes, per tant hem de deixar \textit{64 - sizeof(int)} bytes de \textit{padding}.

\begin{minted}[frame=single, linenos, fontsize=\scriptsize]{c}
int eratosthenes(int lastNumber)
{
	int found = 0;
	int sqrt_lN = sqrt(lastNumber);
	// 1. create a list of natural numbers 2, 3, 4, ... all of them initially marked as potential primes
	char *isPrime = (char *)malloc((lastNumber + 1) * sizeof(char));

	#pragma omp parallel
	#pragma omp single
	#pragma omp taskloop
	for (int i = 0; i <= lastNumber; i++)
		isPrime[i] = 1;

	// 2. Starting from i=2, the first unmarked number on the list ...
	for (int i = 2; i <= sqrt_lN; i++) {
		//for (int i = 2; i*i <= lastNumber; i++)
		// ... find the smallest number greater or equal than i that is unmarked
		if (isPrime[i]) {
			// 3. Mark all multiples of i between i^2 and lastNumber
			#pragma omp parallel
			#pragma omp single
			#pragma omp taskloop
			for (int j = i * i; j <= lastNumber; j += i)
				isPrime[j] = 0;
		}
	}

	// 4. The unmarked numbers are primes, count primes
	struct {
		int num;
		int padding[(64 - sizeof(int)) / 4]; /* Avoid false sharing */
	} *local_found = calloc(omp_get_max_threads(), sizeof(*local_found));
	
	#pragma omp parallel
	#pragma omp single
	#pragma omp taskloop
	for (int i = 2; i <= lastNumber; i++)
		local_found[omp_get_thread_num()].num += isPrime[i];
		
	for (int i = 0; i < omp_get_max_threads(); i++)
		found += local_found[i].num;
	
	free(local_found);

	// 5. We are done with the isPrime array, free it
	free(isPrime);
	return found;
}
\end{minted}

\newpage

\subsection{Evaluació del rendiment}

%\includegraphics[width=1.0\textwidth]{img/sieve1_for.png}
%\includegraphics[width=1.0\textwidth]{img/sieve1_taskloop.png}

Observem que els dos mètodes de paral·lelització en el nostre cas tenen comportaments molt similars en quant a l'escalabilitat:

\begin{figure}[H]
  \centering
  \begin{minipage}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{img/sieve1_for.png}
    \caption{Paral·lelització mitjançant \textit{for}.}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{img/sieve1_taskloop.png}
    \caption{Paral·lelització mitjançant \textit{taskloop}.}
  \end{minipage}
\end{figure}

\section{Paral·lelitzant \textit{sieve2.c}}

\subsection{Estratègies de paral·lelització}

\subsubsection{Utilitzant \textit{for}}

Per tal de paral·lelitzar aquest codi mitjançant \textit{omp for} tenim principalment dues opcions:
\begin{enumerate}
	\item Paral·lelitzar algun dels \textit{for}s interns de \textit{eratosthenesBlock}.
	\item Paral·lelitzar el \textit{for} extern (el que crida a \textit{eratosthenesBlock}).
\end{enumerate}
Cada opció te avantatges i inconvenients: la primera opció va bé quan només es crida a \textit{eratosthenesBlock}, és a dir, quan l'\textit{slice} equival al rang, ja que d'aquesta forma es paral·lelitzarà tot el rang. En canvi la segona opció va bé quan l'\textit{slice} és més petit que el rang, ja que si fossin iguals només hi hauria una iteració i per tant no es podria paral·lelitzar.\par
He decidit triar la segona opció, ja que conjuntament amb el \textit{reduction} la paral·lelització queda molt simple:

\begin{minted}[frame=single, linenos, fontsize=\scriptsize]{c}
int eratosthenes(int lastNumber, int sliceSize)
{
	int found = 0;
	// each slice covers ["from" ... "to"], incl. "from" and "to"
	#pragma omp parallel for reduction(+:found)
	for (int from = 2; from <= lastNumber; from += sliceSize) {
		int to = from + sliceSize;
		if (to > lastNumber)
			to = lastNumber;
		found += eratosthenesBlock(from, to);
	}
	return found;
}
\end{minted}

\subsubsection{Utilitzant \textit{taskloop}}

L'estratègia és molt similar a la de la secció \ref{sieve1:taskloop}, però aquest cop utilitzo \textit{omp atomic} per tal d'actualitzar la variable \textit{found} (en comptes de crear una variable local per cada thread), ja que si es tria un bon \textit{slice} s'hauran de fer relativament poques sumes atòmiques, i per tant no afectarà al rendiment. 

\begin{minted}[frame=single, linenos, fontsize=\scriptsize]{c}
int eratosthenes(int lastNumber, int sliceSize)
{
	int found = 0;
	// each slice covers ["from" ... "to"], incl. "from" and "to"
	#pragma omp parallel
	#pragma omp single
	#pragma omp taskloop
	for (int from = 2; from <= lastNumber; from += sliceSize) {
		int to = from + sliceSize;
		if (to > lastNumber)
			to = lastNumber;
		#pragma omp atomic
		found += eratosthenesBlock(from, to);
	}
	return found;
}
\end{minted}

\subsection{Evaluació del rendiment}

Observem com en la versió seqüencial, l'\textit{slice} més òptim és de \textit{100000}, això podria ser degut a que amb aquest tamany es minimitzen les fallades o expulsions de  \textit{cache}.

\begin{table}[H]
\centering
\label{my-label}
\begin{tabular}{|l|c|c|}
\hline
\textbf{Version/Block size}    & {\textbf{Execution time (s)}} & {\textbf{Speed-up wrt. sieve1-seq}} \\ \hline
sieve1-seq 100000000           & 5.371071                                    & 1                                                     \\
sieve2-seq 100000000 100000000 & 2.973538                                           & 1.80629                                                    \\
sieve2-seq 100000000 10000000  & 1.358952                                           & 3.95236                                                     \\
sieve2-seq 100000000 1000000   & 1.126931                                           & 4.76610                                                     \\
sieve2-seq 100000000 100000    & 0.856866                                           & 6.26827                                                     \\
sieve2-seq 100000000 10000     & 1.474902                                           & 3.64165                                                     \\
sieve2-seq 100000000 1000      & 7.670644                                           & 0.70021 \\ \hline                                                  
\end{tabular}
\end{table}

\begin{figure}[H]
  \centering
  \begin{minipage}[b]{0.45\textwidth}
    \includegraphics[width=0.95\textwidth]{img/sieve2_for.png}
    \caption{Paral·lelització mitjançant \textit{for}.}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{0.45\textwidth}
    \includegraphics[width=0.95\textwidth]{img/sieve2_taskloop.png}
    \caption{Paral·lelització mitjançant \textit{taskloop}.}
  \end{minipage}
\end{figure}

\section{Conclusions}

Hem vist dues maneres de paral·lelitzar un code en C, mitjançant les directives \textit{for} i \textit{taskloop} d'OpenMP. Ja que he intentat imitar el comportament del \textit{for} mitjançant \textit{taskloop}, el comportament en quant a l'escalabilitat i rendiment ha sigut similar, tot i que amb el lleuger \textit{overhead} de la creació i \textit{scheduling} de les tasques.

\end{document}
