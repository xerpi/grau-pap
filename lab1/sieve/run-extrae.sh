#!/bin/bash

USAGE="\n USAGE: ./run-extrae.sh prog range slice_size num_threads\n
        prog        -> omp program name\n
        range       -> count primes between 2 and range\n
        slice_size  -> size of the blocks to sieve (only for sieve2)\n
        num_threads -> number of threads\n"

if (test $# -lt 3 || test $# -gt 3)
#if (test $# -lt 4 || test $# -gt 4)
then
	echo -e $USAGE
        exit 0
fi

export EXTRAE_HOME=/Soft/extrae/3.4.1
export LD_PRELOAD=${EXTRAE_HOME}/lib/libomptrace.so

echo ./$1-omp $2 $3
export OMP_NUM_THREADS=$3
./$1-omp $2 $3

#echo ./$1-omp $2 $3 $4
#export OMP_NUM_THREADS=$4
#./$1-omp $2 $3 $4
