#!/bin/bash

USAGE="\n USAGE: ./run-omp.sh prog range slice_size num_threads\n
        prog        -> omp program name\n
        range       -> count primes between 2 and range\n
        slice_size  -> size of the blocks to sieve (for sieve2)\n
	num_threads -> number of threads\n"

if (test $# -lt 3 || test $# -gt 3)
#if (test $# -lt 4 || test $# -gt 4)
then
	echo -e $USAGE
        exit 0
fi

make $1-omp 

echo /usr/bin/time ./$1-omp $2 $3
/usr/bin/time ./$1-omp $2 $3

#echo /usr/bin/time ./$1-omp $2 $3 $4
#/usr/bin/time ./$1-omp $2 $3 $4
