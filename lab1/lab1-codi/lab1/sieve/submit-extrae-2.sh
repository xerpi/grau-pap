#!/bin/bash
### Directives pel gestor de cues
# following option makes sure the job will run in the current directory
#$ -cwd
# following option makes sure the job has the same environmnent variables as the submission shell
#$ -V
# Canviar el nom del job
#$ -N lab1-extrae
# Per canviar de shell
#$ -S /bin/bash

USAGE="\n USAGE: ./submit-extrae.sh prog range slice_size num_threads\n
        prog        -> omp program name\n
        range       -> count primes between 2 and range\n
        slice_size  -> size of the blocks to sieve (only for sieve2)\n
        num_threads -> number of threads\n"

if (test $# -lt 4 || test $# -gt 4)
#if (test $# -lt 3 || test $# -gt 3)
then
	echo -e $USAGE
        exit 0
fi

make $1-omp

export EXTRAE_HOME=/Soft/extrae/3.4.1
export LD_PRELOAD=${EXTRAE_HOME}/lib/libomptrace.so

#export OMP_NUM_THREADS=$3
#./$1-omp $2 $3

export OMP_NUM_THREADS=$4
./$1-omp $2 $3 $4
